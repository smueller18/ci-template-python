ARG IMAGE_DEV="python:alpine"
FROM $IMAGE_DEV

WORKDIR /app
COPY Pipfile Pipfile.lock /app/

ARG DEPS=""
ARG BUILD_DEPS=""
RUN if [ -n "$DEPS" ]; then apk add --no-cache ${DEPS}; fi && \
  apk add --no-cache --virtual .build-deps \
    g++ \
    git \
    libffi-dev \
    libressl-dev \
    ${BUILD_DEPS} \
    && \
  pip install --no-cache-dir \
    anybadge \
    git+https://gitlab.com/smueller18/pylint-gitlab.git@master \
    jinja2 \
    pipenv \
    pylint \
    && \
  pipenv install --dev --system && \
  apk del --no-cache .build-deps

COPY . /app/
