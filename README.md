# python-ci-tools

Include the badges with follwoing code snippet in your README file:
```
[![coverage](<PATH_TO_YOUR_GITLAB_PROJECT>/badges/master/coverage.svg)](<PATH_TO_YOUR_PROJECT_PAGE>/coverage/)
[![pylint](<PATH_TO_YOUR_PROJECT_PAGE>/lint/pylint.svg)](<PATH_TO_YOUR_PROJECT_PAGE>/lint/)
[![documentation](<PATH_TO_YOUR_PROJECT_PAGE>/documentation.svg)](<PATH_TO_YOUR_PROJECT_PAGE>/)
```
